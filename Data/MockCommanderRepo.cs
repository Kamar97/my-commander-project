using System.Collections.Generic;
using Commander.Models;

namespace Commander.Data{


    public class MockCommanderRepo : ICommanderRepo //here're we're implemeting the interface that we created thats ICommanderRepo
    {
        public IEnumerable<Command> GetAllCommands()
        {
            var commands=new List <Command>{
                new Command{Id=0,HowTo="Boil an egg",Line="Boil Water",Platform="kettle and pan"},
                new Command{Id=1,HowTo="Boil a chicken",Line="Boil chicky water",Platform="pan and spoon"},
                new Command{Id=2,HowTo="eat chocolate ",Line="bring chocolate",Platform="spoon and plate"},

            };
            return commands;
        }

        public Command GetCommandById(int id)
        {
            return new Command{Id=0,HowTo="Boil an egg",Line="Boil Water",Platform="kettle and pan"};//these are the attributes we presented in the model
        }
    }







}
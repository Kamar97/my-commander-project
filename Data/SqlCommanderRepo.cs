using System.Collections.Generic;         //In this class we implemented our interface
using System.Linq;
using Commander.Models;

namespace Commander.Data{

    public class SqlCommanderRepo : ICommanderRepo

    {


        public SqlCommanderRepo(CommanderContext context)
        {
            _context=context;//generate a readonly field
        }

        public CommanderContext _context { get; }

        public IEnumerable<Command> GetAllCommands( )
        {
            return _context.commands.ToList();//use its needed namespace,here we're using commands as our databse set
        }

        public Command GetCommandById(int id)
        {
          return _context.commands.FirstOrDefault(p=>p.Id==id);
        }
    }







}
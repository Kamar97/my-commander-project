using System.Collections.Generic;
using Commander.Data;
using Commander.Models;
using Microsoft.AspNetCore.Mvc;

namespace Commands.Controllers {



[Route("Api/commands")]  //its how we get to the resources and how do we get to the API endpoints
[ApiController]



public class CommandsController:ControllerBase  //this controller inherits from the C=controllerBase which is an MVC class 

{
        private readonly ICommanderRepo _repository;

        public CommandsController(ICommanderRepo repository)//press ctor to create a constructor
    {

     _repository=repository;

        
    }
//private readonly MockCommanderRepo _repository=new MockCommanderRepo();
 // Get /api/commands
[HttpGet]
public ActionResult <IEnumerable<Command>> GetAllCommands(){
var commandItems =_repository.GetAllCommands();

return Ok(commandItems);

    
}//second endpoint

//Get api/commands/{id}
[HttpGet("{id}")]
public ActionResult <Command>GetCommandById(int id){//this id comes from the request from the url in postman
    var commandItem=_repository.GetCommandById(id);

    return Ok (commandItem);


}//first endpoint





} 








}